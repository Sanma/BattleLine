﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultText : MonoBehaviour
{
    [SerializeField] Button button_;
    private Color defaultColor_;

    // テキスト
    private Text _text;


    // コンストラクタ
    private void Start()
    {
        _text = this.GetComponent<Text>();
        _text.text = FlagManager.GetWinTeam().ToString()+" Win!!";

        defaultColor_ = button_.image.color;
    }


    public void MouseOver()
    {
        button_.image.color = new Color( 0.5f, 0.5f, 0.5f, 0.5f );
    }

    public void MouseRelease()
    {
        button_.image.color = defaultColor_;
    }

    public void OnClickTitle()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene( "TitleScene" );
    }
}