﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Title : MonoBehaviour
{
    [SerializeField] AudioSource audioSourse_;
    [SerializeField] AudioClip se_;

    [SerializeField] Button button_;
    private Color defaultColor_;
    private void Start()
    {
        defaultColor_ = button_.image.color;
    }

    public void OnClickStart()
    {
        audioSourse_.PlayOneShot( se_ );

        UnityEngine.SceneManagement.SceneManager.LoadScene( "SelectScene" );
    }

    public void MouseOver()
    {
        button_.image.color = new Color( 0.5f, 0.5f, 0.5f, 0.5f );
    }

    public void MouseRelease()
    {
        button_.image.color = defaultColor_;
    }
}