﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuleImageChange : MonoBehaviour
{
    [SerializeField] Texture2D[] ruleImages_;

    public int currentNumber_;

    [SerializeField] RawImage image_;


    private void Start()
    {
        image_ = this.GetComponent<RawImage>();

        image_.texture = ruleImages_[0];

        currentNumber_ = 0;
    }


    private void Update()
    {
        if( image_ != ruleImages_[currentNumber_] )
        {
            image_.texture = ruleImages_[currentNumber_];
        }
    }


    public void RuleImageBack()
    {
        if( currentNumber_ != 0 )
        {
            --currentNumber_;
        }
    }


    public void RuleImageNext()
    {
        if( currentNumber_ != ruleImages_.Length-1 )
        {
            currentNumber_++;
        }
    }
}