﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectTarget : MonoBehaviour
{
    [SerializeField] CardMove cardMove_;

    [SerializeField] RawImage cardSelectImage_;
    [SerializeField] RawImage fieldSelectImage_;

    [SerializeField] Text cardSelect_;
    [SerializeField] Text fieldSelect_;

    float alpha_;

    float blinkCount_;

    private void Start()
    {
        blinkCount_ = 0;
    }

    private void Update()
    {
        alpha_ = Mathf.Sin( blinkCount_ )/2+0.5f;

        if( ( !cardMove_.isCardDecide_  )
        &&  ( !cardMove_.isFieldDecide_ )
        ){
            CardSelectGuide();
        }

        if( (  cardMove_.isCardDecide_  )
        &&  ( !cardMove_.isFieldDecide_ )
        ){
            FieldSelectGuide();
        }

        if( (  cardMove_.isCardDecide_  )
        &&  (  cardMove_.isFieldDecide_ )
        ){
            NonSelectGuide();
        }

        blinkCount_ += 0.1f;
    }

    private void CardSelectGuide()
    {
        if( ( !cardSelect_.isActiveAndEnabled  )
        ||  (  fieldSelect_.isActiveAndEnabled )
        ){
            cardSelect_.enabled = true;
            fieldSelect_.enabled = false;
            blinkCount_ = 0;
        }

        cardSelect_.color = new Color( cardSelect_.color.r, cardSelect_.color.g, cardSelect_.color.b, alpha_ );
    }


    private void FieldSelectGuide()
    {
        if( (  cardSelect_.isActiveAndEnabled  )
        ||  ( !fieldSelect_.isActiveAndEnabled )
        ){
            cardSelect_.enabled = false;
            fieldSelect_.enabled = true;
            blinkCount_ = 0;
        }

        fieldSelect_.color = new Color( fieldSelect_.color.r, fieldSelect_.color.g, fieldSelect_.color.b, alpha_ );
    }


    private void NonSelectGuide()
    {
        if( ( cardSelect_.isActiveAndEnabled  )
        ||  ( fieldSelect_.isActiveAndEnabled )
        ){
            cardSelect_.enabled = false;
            fieldSelect_.enabled = false;
            blinkCount_ = 0;
        }
    }
}