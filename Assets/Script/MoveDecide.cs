﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDecide : MonoBehaviour
{
    [SerializeField] AudioSource audioSourse_;
    [SerializeField] AudioClip se_;

    // カードマネージャ
    private CardManager cardManager_;

    // カードチェック
    private FieldData fieldData_;

    // カード移動
    private CardMove cardMove_;

    // コンストラクタ
    private void Start()
    {
        cardManager_ = GameObject.FindWithTag( "CardManager" ).GetComponent<CardManager>();

        fieldData_ = this.GetComponent<FieldData>();

        cardMove_ = GameObject.FindWithTag( "CardMove" ).GetComponent<CardMove>();
    }

    public void OnClickField()
    {
        if( !cardMove_.isCardDecide_ )
        {
            return;
        }

        if( ( fieldData_.team_ == cardManager_.nowTurn_ )
        &&  ( fieldData_.transform.childCount < 3 )
        ){
            audioSourse_.PlayOneShot( se_ );

            cardMove_.IsFieldDecide = true;
            cardMove_.SetFieldData( fieldData_ );
        }
    }
}
