﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Experimental.PlayerLoop;


// カードの色
public enum CardColor
{
    Red,        // 赤
    Orange,     // 橙
    Purple,     // 紫
    Blue,       // 青
    Green,      // 緑
    Yellow,     // 黄
    Max         // MAX
}


public class CardData : MonoBehaviour
{
    private CardManager cardManager_;

    // 色
    public CardColor _color;

    // 数字
    public int _number;

    // カード持ち主
    public Team _team;

    public int imageNum_;

    private Color defaultColor_;

    [SerializeField] Image image_;

    // デフォルトコンストラクタ
    public CardData()
    {
        _color  = CardColor.Max;
        _number = CardManager.CardMaxNum;
        _team   = Team.Non;
    }


    // 引数付きコンストラクタ
    public CardData( CardColor color, int number, Team team )
    {
        _color  = color;
        _number = number;
        _team   = team;
    }


    public void Update()
    {
        if ( cardManager_ == null )
        {
            cardManager_ = GameObject.FindWithTag( "CardManager" ).GetComponent<CardManager>();
        }

        if( this.transform.parent == cardManager_._opponentHandTransform )
        {
            image_.sprite = cardManager_._cardImages[60];
        }
        else
        {
            image_.sprite = cardManager_._cardImages[imageNum_];
        }
    }


    public void MouseOver()
    {
        defaultColor_ = image_.color;

        if ( _team != Team.Non )
        {
            image_.color = new Color( 1.0f, 1.0f, 1.0f, 0.5f );
        }
    }


    public void MouseRelease()
    {
        image_.color = defaultColor_;
    }
}