﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class FieldData : MonoBehaviour
{
    // 陣営
    public Team team_;

    [SerializeField] RawImage image_;

    // 置かれているカード数
    public int setCardNum_;

    private Color defaultColor_;

    // コンストラクタ
    public FieldData( Team team )
    {
        team_ = team;
        setCardNum_ = 0;
    }

    public void Update()
    {
        this.transform.position = new Vector3( this.transform.position.x, this.transform.position.y, 1000 );
    }

    public void MouseOver()
    {
        defaultColor_ = image_.color;

        if( team_ == Team.Player )
        {
            image_.color = new Color( 1.0f, 1.0f, 1.0f, 0.5f );
        }
    }


    public void MouseRelease()
    {
        image_.color = defaultColor_;
    }
}
