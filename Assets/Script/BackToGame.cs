﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToGame : MonoBehaviour
{
    [SerializeField] Canvas ruleCanvas_;

    [SerializeField] RuleImageChange ruleImageChange_;

    public void OnClick()
    {
        ruleImageChange_.currentNumber_ = 0;

        ruleCanvas_.enabled = false;
    }
}