﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CardDecide:MonoBehaviour
{
    [SerializeField] AudioSource audioSourse_;
    [SerializeField] AudioClip se_;

    // カードマネージャ
    private CardManager cardManager_;

    // カードデータ
    CardData cardData_;

    // カード移動
    private CardMove cardMove_;

    // コンストラクタ
    private void Start()
    {
        cardManager_ = GameObject.FindWithTag( "CardManager" ).GetComponent<CardManager>();

        cardData_ = this.GetComponent<CardData>();

        cardMove_ = GameObject.FindWithTag( "CardMove" ).GetComponent<CardMove>();
    }

    public void OnClickCard()
    {
        if( cardData_._team == cardManager_.nowTurn_ )
        {
            audioSourse_.PlayOneShot( se_ );

            cardMove_.IsCardDecide = true;
            cardMove_.SetCardData( cardData_ );
        }
    }
}