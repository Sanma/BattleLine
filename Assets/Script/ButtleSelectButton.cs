﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtleSelectButton : MonoBehaviour
{
    private Button button_;
    private Color defaultColor_;

    // シーンセレクト
    private SceneManager _sceneManager;

    // コンストラクタ
    private void Start()
    {
        button_ = GetComponent<Button>();

        var sceneManager = GameObject.FindGameObjectWithTag( "SceneManager" );

        _sceneManager = sceneManager.GetComponent<SceneManager>();

        defaultColor_ = button_.image.color;
    }


    public void OnClick()
    {
        _sceneManager.SetBattleMode( (BattleMode)Enum.Parse( typeof( BattleMode ), this.name ) );
    }


    public void MouseOver()
    {
        button_.image.color = new Color( 0.5f, 0.5f, 0.5f, 0.5f );
    }

    public void MouseRelease()
    {
        button_.image.color = defaultColor_;
    }
}
