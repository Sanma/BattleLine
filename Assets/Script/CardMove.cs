﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardMove:MonoBehaviour
{
    // カードデータ
    private CardData cardData_;

    // フィールドデータ
    private FieldData fieldData_;

    // カードマネージャ
    private CardManager cardManager_;

    // カード決定
    public bool isCardDecide_;

    // フィールド決定
    public bool isFieldDecide_;

    // 移動中
    private bool isMove_;

    // コンストラクタ
    private void Start()
    {
        cardManager_ = GameObject.FindWithTag( "CardManager" ).GetComponent<CardManager>();

        isCardDecide_   = false;
        isFieldDecide_  = false;
        isMove_ = false;
    }


    // アップデート
    private void Update()
    {
        if( ( isCardDecide_     )
        &&  ( isFieldDecide_    )
        ){
            isMove_ = true;
        }

        if( isMove_ )
        {
            isMove_ = false;
            isCardDecide_ = false;
            isFieldDecide_ = false;

            var hand = cardData_.transform.parent;

            fieldData_.setCardNum_++;

            cardData_.transform.parent = fieldData_.transform;

            cardData_._team = Team.Non;

            if ( cardManager_.GetStrageTopNum() < cardManager_.GetMaxNum() )
            {
                cardManager_.CardInstantiate( hand );
            }

            cardManager_.nowTurn_ = 1-cardManager_.nowTurn_;
        }
    }


    public void SetCardData( CardData cardData )
    {
        cardData_ = cardData;
    }


    public void SetFieldData( FieldData fieldData )
    {
        fieldData_ = fieldData;
    }


    // isDecide_プロパティ
    public bool IsCardDecide
    {
        get
        {
            return isCardDecide_;
        }

        set
        {
            isCardDecide_ = value;
        }
    }


    // isMove_プロパティ
    public bool IsFieldDecide
    {
        get
        {
            return isFieldDecide_;
        }

        set
        {
            isFieldDecide_ = value;
        }
    }
}