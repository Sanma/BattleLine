﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickRule : MonoBehaviour
{
    [SerializeField] Canvas ruleCanvas_;

    private void Start()
    {
        ruleCanvas_.enabled = false;
    }


    public void OnClick()
    {
        ruleCanvas_.enabled = true;
    }
}
