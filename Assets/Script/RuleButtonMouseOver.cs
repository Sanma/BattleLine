﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuleButtonMouseOver : MonoBehaviour
{
    private Button button_;
    private Color defaultColor_;


    private void Start()
    {
        button_ = GetComponent<Button>();
        defaultColor_ = button_.image.color;
    }


    public void MouseOver()
    {
        button_.image.color = new Color( 0.5f, 0.5f, 0.5f, 0.5f );
    }


    public void MouseRelease()
    {
        button_.image.color = defaultColor_;
    }
}
